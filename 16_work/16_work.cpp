﻿#include <iostream>
#include <iomanip>
#include <ctime>
using namespace std;
int const NN = 20;

void Create_Mass()

{

	cout << "Enter the dimension of a square matrix (20 - max): ";
	int Dim_om;
	cin >> Dim_om;
	while ((Dim_om <= 0) || (Dim_om > 20))
	{
		cout << "Try again: ";
		cin >> Dim_om;
	}


	time_t t = time(0);
	tm Localtime;
	localtime_s(&Localtime, &t);


	int Mass[NN][NN];
	for (int i = 0; i < (Dim_om ); i++)
	{
		for (int j = 0; j < (Dim_om ); j++)
		{
			Mass[i][j] = i + j;
		}
	}

	for (int i = 0; i < (Dim_om ); i++)
	{
		for (int j = 0; j < (Dim_om ); j++)
		{
			cout << setw(4) << Mass[i][j];
		}
		cout << endl;
	}

	int Sum = 0;
	cout << "Enter the N: ";
	int N;
	cin >> N;
	int IndexSum = (&Localtime)->tm_mday % N;
	for (int j = 0; j < (Dim_om ); j++)
	{
		Sum += Mass[IndexSum][j];
	}

	cout << "String # " << IndexSum << endl << "Sum = " << Sum << endl;




}

int main()
{
	Create_Mass();
}